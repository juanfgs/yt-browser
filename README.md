# yt-browser
A GTK3 interface for youtube-dl

This project came is intended to be a easy to use interface for youtube-dl. The goal is to have a streamlined UI which is straightforward to use, more options will be added but in a way the main UI and usage remains simple and intuitive.

Installation
------
You can install this project by running

~~~
  python setup.py install
~~~

Then run it with "ytbrowser"

There are also [deb](https://github.com/juanfgs/yt-browser/raw/master/deb_dist/python3-ytbrowser_0.2.1-1_all.deb) and [rpm](https://github.com/juanfgs/yt-browser/raw/master/dist/YTBrowser-0.2.1-1.noarch.rpm) packages. 

Dependencies
------------

This software depends on google's youtube API for Python, PyGobject and Youtube-DL

User Interface
----------
![Main window](https://pbs.twimg.com/media/DEVl7mJXsAAOJT5.png:large)

TODO:

- Create installable package
- Add search suggestions as you type
- Improve UI (specially for the video list)
- Add configuration options (conversion formats, bitrate)
