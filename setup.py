from setuptools import setup

setup(name='YTBrowser',
      version='1.1.1',
      description='Youtube Video Downloader',
      author='Juan F. Gimenez Silva',
      license='GPLv3',
      author_email='juanfgs@openmailbox.org',
      url='http://juanfgs.com',
      setup_requires=[],
      tests_require=['pytest','pytest-runner'],
      install_requires=['pygobject','pycairo','google-api-python-client','youtube-dl','notify2'],
      package_dir={'' : 'src'},
      packages=['YTBrowser','YTBrowser.backends','YTBrowser.models','YTBrowser.controllers','YTBrowser.downloaders','YTBrowser.views','YTBrowser.conf','YTBrowser.views.gtk.threads'],
      scripts=['src/ytbrowser'],
      data_files=[
          ('share/YTBrowser/resources/ui/gtk', ['src/YTBrowser/resources/ui/gtk/YTBrowser.ui']),
          ('share/doc/YTBrowser/resources', ['LICENSE']),
          ('share/YTBrowser/resources/icons', ['src/YTBrowser/resources/icons/search.png','src/YTBrowser/resources/icons/not-found.png']),
          ('share/applications', ['src/YTBrowser/resources/YTBrowser.desktop']),
          ('share/icons/hicolor/scalable/apps', ['src/YTBrowser/resources/YTBrowser.svg']),
      ]
)
