from YTBrowser.backends.backend import Backend
from apiclient.discovery import build
from apiclient.errors import HttpError
from oauth2client.tools import argparser


class Youtube(Backend):
    def _connect(self, http=None):
        """
        Connects to Youtube API, using current settings
        Parameters
        ----------
        http: HttpMock, optional
        if needed one can supply an HttpMock object for testing purposes
        """
        self.service = build(self.settings['serviceName'],
                             self.settings['apiVersion'],
                             developerKey=self.settings['developerKey'],
                             http=http,
                             cache_discovery=False)

    def _search(self, term, http=None):
        """
        Search a video
        Parameters
        ----------
        term: str, required
        The term to search for

        http: HttpMock, optional
        if needed one can supply an HttpMock object for testing purposes
        """
        results = {'videos':  [],
                   'channels': [],
                   'playlists': []}
        self.__request = self.service.search().list(
            q=term,
            part="id,snippet",
            maxResults=10
        )
        if(http):
            response = self.__request.execute(http)
        else:
            response = self.__request.execute()

        for item in response.get("items", []):
            if item['id']['kind'] == 'youtube#video':
                video = {'id': item['id']['videoId'],
                         'title': item['snippet']['title'],
                         'description': item['snippet']['description'],
                         'thumbnail': item['snippet']['thumbnails']['default']['url'],
                         'channel': {'id': item['snippet']['channelId'],
                                     'name': item['snippet']['channelTitle']}}
                results['videos'].append(video)
        return results
