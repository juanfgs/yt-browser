class Backend():
    def __init__(self):
        self.errors = []
        self.settings = {}

    def search(self, term):
        return self._search(term)

    def connect(self):
        self._connect()

    def set_settings(self, settings):
        """
        Sets the settings for the backend
        Parameters
        ----------
        settings: dict, required
        a dict with the needed settings
        """
        self.settings = settings
