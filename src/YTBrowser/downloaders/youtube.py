import youtube_dl


class Youtube():

    def __init__(self, settings):
        self.settings = settings

    def download(self, id):
        self.id = id
        with youtube_dl.YoutubeDL(self.settings) as ydl:
            ydl.download(["http://www.youtube.com/watch?v=" + id])
