import pytest
from YTBrowser.backends.backend import Backend


@pytest.fixture
def backend():
    backend = Backend()
    return backend


def test_set_settings(backend):
    backend.set_settings({'api_key': 'foobar'})
    assert 'foobar' == backend.settings['api_key']
