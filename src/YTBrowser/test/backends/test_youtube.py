import pytest
from YTBrowser.backends.youtube import Youtube
from apiclient.http import HttpMock
from apiclient.discovery import Resource


@pytest.fixture
def settings():
    return {'serviceName': 'youtube',
            'apiVersion': 'v3',
            'developerKey': 'a-key'}


result = {'id': 'AAAAAAAAAAA',
          'title': 'A Video',
          'description': 'A Description',
          'thumbnail': 'https://i.ytimg.com/vi/AAAAAAAAAAA/default.jpg',
          'channel': {'id': 'UCq-AAAAAAAAAAA',
                      'name': 'ChannelName'}}


@pytest.fixture
def backend(settings):
    backend = Youtube()
    backend.set_settings(settings)
    return backend


def test_connect(backend):
    backend._connect(HttpMock('src/YTBrowser/test/backends/youtube-discovery.json',
                              {'status': '200'}))
    assert isinstance(backend.service, Resource)


def test_search(backend):
    backend._connect(HttpMock('src/YTBrowser/test/backends/youtube-discovery.json',
                              {'status': '200'}))
    result = backend._search("Python",
                             HttpMock('src/YTBrowser/test/backends/youtube-search.json',
                                      {'status': '200'}))
    assert result['videos'][0] == result['videos'][0]
