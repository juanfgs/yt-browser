# -*- coding: utf-8 -*-
# YT-Browser is a GTK3 user interface for youtube-dl
#
# Copyright (C) 2017 Juan Francisco Giménez Silva
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from YTBrowser.conf.configuration import Configuration
from YTBrowser.backends.youtube import Youtube
import os
import sys


class YTBrowser():
    def __init__(self, ui):
        self.config_file = os.path.expanduser(
            "~/.config/YTBrowser/config.json")
        conf = Configuration(self.config_file,
                             {
                                 'serviceName': 'youtube',
                                 'apiVersion': 'v3', 
                                 'developerKey': 'AIzaSyDFuK00HWV0fd1VMb17R8GghRVf_iQx9uk',
                                 'format': 'mkv',
                                 'preferredcodec': 'mp3',
                                 'preferredquality': '256',
                                 'quality': 'bestvideo'
                             })
        self.backend = Youtube()
        self.backend.set_settings(conf.settings)
        self.backend.connect()
        from YTBrowser.views import gtkui
        self.view = gtkui.GtkUi(self.backend)

    def run(self):
        self.view.run(sys.argv)
