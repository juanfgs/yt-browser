# -*- coding: utf-8 -*-

# YT-Browser is an user interface for youtube-dl
#
# Copyright (C) 2017 Juan Francisco Giménez Silva
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


class Video:
    def __init__(self, id, name, filename):
        self.id = id
        self.name = name
        self.filename = filename
        self.progress = 0.0
        self.metadata = {}
        self.status = "Descargando"
