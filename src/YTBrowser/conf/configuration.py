# -*- coding: utf-8 -*-

# YT-Browser is a GTK3 user interface for youtube-dl
#
# Copyright (C) 2017 Juan Francisco Giménez Silva
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import json
import os

class Configuration():

    def __init__(self,path,defaultSettings):
        self.config_dir = os.path.dirname(path)
        self.config_file = path

        if not os.path.exists(self.config_dir):
            os.makedirs(self.config_dir)

        if not os.path.isfile(self.config_file):
            self._settings = defaultSettings
            with open(self.config_file, 'w') as settings_file:
                json.dump(self._settings, settings_file)
        else:
            with open(path, 'r') as ymlfile:
                self._settings = json.load(ymlfile)


    @property
    def settings(self):
        return self._settings

    @settings.setter
    def settings(self, options):
        self._settings = options
        with open(self.config_file, 'w') as settings_file:
            json.dump(self._settings, settings_file)
