# -*- coding: utf-8 -*-
# YT-Browser is a GTK3 user interface for youtube-dl
#
# Copyright (C) 2017 Juan Francisco Giménez Silva
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gi
import notify2
from pprint import pprint
from YTBrowser.models.video import Video
from YTBrowser.views.gtk.threads.search_thread import SearchThread
from YTBrowser.views.gtk.threads.thumbnails_thread import ThumbnailsThread
from YTBrowser.views.gtk.threads.download_thread import DownloadThread

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, Gio, GLib
from gi.repository.GdkPixbuf import Pixbuf
from gi.repository.GdkPixbuf import InterpType


class GtkUi(Gtk.Application):

    def __init__(self, backend, *args, **kwargs):
        self.video_settings = {'format': 'bestvideo+bestaudio'}
        super().__init__(*args, application_id="org.gnome.ytbrowser",
                         flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE,
                         **kwargs)
        notify2.init("Gestor de videos")
        self.path = "/usr/share/"
        self.window = None
        self.builder = self.initialize_builder(self.path)
        self.video_search = self.builder.get_object("VideoSearch")
        self.video_list = self.builder.get_object("VideoList")
        self.stack = self.builder.get_object("MainStack")
        self.download_list_store = self.builder.get_object("DownloadListStore")
        self.download_popover = self.builder.get_object("DownloadsPopOver")
        self.video_download_list = []
        self.current_page_thumbnail_urls = []
        self.main_backend = backend

    def initialize_builder(self, path):
        builder = Gtk.Builder()
        builder.add_from_file(path +
                              'YTBrowser/resources/ui/gtk/YTBrowser.ui')
        builder.connect_signals(self)
        return builder

    def do_startup(self):
        """ Initialize Gtk.Application Framework """
        Gtk.Application.do_startup(self)

    def do_command_line(self, command_line):
        options = command_line.get_options_dict()

        if options.contains("test"):
            # This is printed on the main instance
            print("Test argument recieved")

        self.activate()
        return 0

    def do_activate(self):
        if not self.window:
            self.window = self.builder.get_object("MainWindow")
            self.window.set_application(self)

        self.window.present()

    def get_license(self):
        with open('/usr/share/doc/YTBrowser/resources/LICENSE') as license:
            return license.read()

    @property
    def backend(self):
        return self.main_backend

    @backend.setter
    def backend(self, value):
        self.main_backend = value

    # Signals
    def send_results(self, *args):
        pprint(args)

    # Common Actions
    def show_video_area(self):
        scrolled_window = self.builder.get_object("VideoListScrolledWindow")
        self.video_list.show()
        self.stack.set_visible_child(scrolled_window)

    def video_item_add(self, result):
        builder = self.initialize_builder(self.path)
        video_item = builder.get_object("VideoArea")
        video_title = builder.get_object("VideoInfo")
        download_button = builder.get_object("DownloadButton")
        convert_button = builder.get_object("ConvertButton")
        video_title.set_text(result["title"])
        download_button.connect("clicked",
                                self.on_download_clicked,
                                {"video_id": result["id"],
                                 "title": result["title"],
                                 "action": "download"})
        convert_button.connect("clicked",
                               self.on_convert_clicked,
                               {"video_id": result["id"],
                                "title": result["title"],
                                "action": "convert"})

        self.video_list.add(video_item)
        self.current_page_thumbnail_urls.append(result["thumbnail"])

    def display_thumbnails(self, thumbnails):
        for idx, thumbnail in enumerate(thumbnails):
            stream = Gio.MemoryInputStream.new_from_data(thumbnail.read(), None)
            pixbuf = Pixbuf.new_from_stream(stream,None)
            pixbuf = pixbuf.scale_simple(128, 100, InterpType.BILINEAR)
            thumbnail_image = Gtk.Image().new_from_pixbuf(pixbuf)
            video_item = self.video_list.get_row_at_index(idx).get_child()
            video_item.pack_start(thumbnail_image, True, False, 0)
            thumbnail_image.show()
        self.show_video_area()

    def display_results(self, results):
        self.current_page_thumbnail_urls = []
        for result in results.get("videos", []):
                        self.video_item_add(result)
        search_thread = ThumbnailsThread(self.current_page_thumbnail_urls)
        search_thread.run(self.display_thumbnails)

    def add_to_download_list_store(self, video):
        iter = self.download_list_store.append([video.name, video.status, 0])
        return iter

    def update_progressbar(self, progress):
        for video in self.video_download_list:
            if video.filename in progress["filename"]:
                if 'total_bytes_estimate' in progress:
                    self.download_list_store[video.index][2] = progress['downloaded_bytes'] / progress['total_bytes_estimate'] * 100
                else:
                    self.download_list_store[video.index][2] = progress['downloaded_bytes'] / progress['total_bytes'] * 100
                self.download_list_store[video.index][1] = progress["status"]
                video.status = progress["status"]

    def notify_download(self, filename):
        """ notify the user that the download is complete """
        n = notify2.Notification("Video descargado", "%s ha terminado de descargarse" % filename )
        n.show()

    ### Callbacks ###
    def on_quit_action_activate(self, action):
        self.quit()

    def on_about_action_activate(self, action):
        about_dialog = Gtk.AboutDialog(self.window, self.window, True)
        about_dialog.set_version("1.1.0")
        about_dialog.set_logo(Pixbuf.new_from_file("/usr/share/icons/hicolor/scalable/apps/YTBrowser.svg"))
        about_dialog.set_copyright("Copyright © 2019 Juan F. Giménez Silva")
        about_dialog.set_license(self.get_license())
        about_dialog.set_authors(["Juan Francisco Giménez Silva <juanfgs@gmail.com>"])
        about_dialog.set_default_response(Gtk.ResponseType.CLOSE)
        response = about_dialog.run()
        about_dialog.destroy()

    def on_video_search_activated(self, widget):
        searching_message = self.builder.get_object("SearchingMessage")
        # iterate through the list items and remove child items
        self.video_list.foreach(lambda child: self.video_list.remove(child))
        self.stack.set_visible_child(searching_message)
        search_thread = SearchThread(self.backend,
                                     self.video_search.get_text())
        search_thread.run(self.display_results)

    def on_download_clicked(self, button, video_data):
        self.video_settings['format'] = 'bestvideo+bestaudio'
        self.queue_download(video_data)

    def on_convert_clicked(self, button, video_data):
        '''
        When convert is pushed we pass some parameters to the backend
        TODO: parametrize bitrate and format
        '''
        self.video_settings['format'] = 'bestaudio'
        self.video_settings['postprocessors'] = [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '192',
        }]

        self.queue_download(video_data)

    def queue_download(self, video_data):
        """ Show the file dialog """
        file_chooser_dialog = Gtk.FileChooserDialog(
            "Elegir donde guardar el video",
            self.window,
            Gtk.FileChooserAction.SAVE,
            (Gtk.STOCK_CANCEL,
             Gtk.ResponseType.CANCEL,
             Gtk.STOCK_SAVE,
             Gtk.ResponseType.OK))

        file_chooser_dialog.set_current_name(video_data["title"])
        if file_chooser_dialog.run() == Gtk.ResponseType.OK:
            filename = file_chooser_dialog.get_filename()
            print(filename)
            file_chooser_dialog.destroy()
            video = Video(video_data["video_id"],
                          video_data["title"],
                          filename)
            video.index = self.add_to_download_list_store(video)
            self.video_download_list.append(video)
            self.download_popover.popup()
            self.video_settings['outtmpl'] = filename + ".%(ext)s"
            download_thread = DownloadThread(self.video_settings,
                                             video_data["video_id"],
                                             filename,
                                             self.update_progressbar)
            download_thread.run(self.notify_download)
        else:
            file_chooser_dialog.destroy()

    def on_clear_downloaded_clicked(self, button):
        for video in self.video_download_list:
            if video.status == "finished":
                self.download_list_store.remove(video.index)
                self.video_download_list.remove(video)
