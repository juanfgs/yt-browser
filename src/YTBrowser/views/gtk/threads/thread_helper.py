from threading import Thread
class ThreadHelper():
    def run(self, callback):
        self.callback = callback
        self.download_thread = Thread(target=self.process)
        self.download_thread.daemon = True
        self.download_thread.start()
