from gi.repository import GLib
from YTBrowser.views.gtk.threads.thread_helper import ThreadHelper

class SearchThread(ThreadHelper):
    def __init__(self,backend,term):
        self.backend = backend
        self.term = term

    def process(self):
        results = self.backend.search(self.term)
        GLib.idle_add(self.callback, results)
