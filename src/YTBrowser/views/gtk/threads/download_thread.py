from gi.repository import GLib
from YTBrowser.views.gtk.threads.thread_helper import ThreadHelper
from YTBrowser.downloaders.youtube import Youtube


class DownloadThread(ThreadHelper):
    def __init__(self, settings, video_id, filename, progress_cb):
        self.settings = settings
        self.filename = filename
        self.id = video_id
        self.progress_cb = progress_cb
        self.status = ""

    def process(self):
        self.settings["progress_hooks"] = [self.progress_cb]
        downloader = Youtube(self.settings)
        downloader.download(self.id)
        GLib.idle_add(self.callback, self.filename)
