from gi.repository import GLib
from urllib import request
from YTBrowser.views.gtk.threads.thread_helper import ThreadHelper


class ThumbnailsThread(ThreadHelper):
    def __init__(self, thumbnails_list):
        self.thumbnails_list = thumbnails_list

    def process(self):
        thumbnails = []
        for image_url in self.thumbnails_list:
            thumbnails.append(request.urlopen(image_url))

        GLib.idle_add(self.callback, thumbnails)
